// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// Це механізм, який дозволяє об'єктам використовувати властивості та методи інших об'єктів. Кожен об'єкт має прототип, який може бути іншим об'єктом або значенням null.

// Для чого потрібно викликати super() у конструкторі класу-нащадка?

// У цьому конструкторі super() слугує для виклику конструктора батьківського класу.Основна причина полягає в тому, що це дозволяє правильно налаштувати об'єкт, створений класом-нащадком, відповідно до конструктора батьківського класу.


// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
// Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    get name() {
      return this._name;
    }
  
    set name(value) {
      if (typeof value !== 'string' || !value.trim()) {
        throw new Error('Invalid name');
      }
      this._name = value;
    }
  
    get age() {
      return this._age;
    }
  
    set age(value) {
      if (typeof value !== 'number' || value < 18 || value > 65) {
        throw new Error('Invalid age');
      }
      this._age = value;
    }
  
    get salary() {
      return this._salary;
    }
  
    set salary(value) {
      if (typeof value !== 'number' || value < 0) {
        throw new Error('Invalid salary');
      }
      this._salary = value;
    }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
    }
  
    get salary() {
      return super.salary * 3;
    }
  
    get lang() {
      return this._lang;
    }
  
    set lang(value) {
      this._lang = value;
    }
  }
  
  const programmer1 = new Programmer('Ivan', 29, 500000, ['JavaScript', 'Ruby']);
  console.log(programmer1.name); 
  console.log(programmer1.age);
  console.log(programmer1.salary); 
  console.log(programmer1.lang); 
  
  const programmer2 = new Programmer('Andrii', 33, 600000, ['Java', 'Python']);
  console.log(programmer2.name); 
  console.log(programmer2.age); 
  console.log(programmer2.salary); 
  console.log(programmer2.lang);

  const programmer3 = new Programmer ( 'Yaroslav',28, 450000, ['C++'] ) ;
  console.log(programmer3.name);
  console.log(programmer3.age);
  console.log(programmer3.salary);
  console.log(programmer3.lang);